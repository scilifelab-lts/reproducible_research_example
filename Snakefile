rule report:
    input:
        expand("intermediate/{{sra_id}}_{dirs}_fastqc.zip",dirs=["1","2"])
    output:
        "results/{sra_id}.pdf"
    params:
        script="source/sra_report.Rmd"
    shell:
        "for z in {input}; do unzip $z -d intermediate; done;"
        "echo \"library(rmarkdown);render(input = '{params.script}',output_dir = \
        dirname('{output}'),output_file = basename('{output}'),params = \
        list(sra_id='{wildcards.sra_id}',run_directory='../intermediate'),output_format = pdf_document(toc = F))\" \
        | R --vanilla;"

rule fastQC:
    input:
        "{prefix}.fastq.gz"
    output:
        temp("{prefix}_fastqc.zip")
    shadow: "shallow"
    shell:
        "fastqc -q {input}"

rule get_SRA_by_accession:
    output:
        expand("intermediate/{{sra_id}}_{dirs}.fastq.gz",dirs=["1","2"])
    run:
        baseDir=wildcards.sra_id[0:6]
        shell("wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/"+baseDir+"/{wildcards.sra_id}/* -P intermediate/")