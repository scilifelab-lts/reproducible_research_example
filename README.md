To reproduce all results in the presentation\*, choose one of the options below (A-E).

*\*) Please note that changes to the code (primarily in `Dockerfile` and `conda_env.yml`) have been made since the original ppt presentation. Therefore, the examples is the presentation should be seen as out of date in terms of code detail, yet conceptually still fine. You can access the code that is paired with the presentation through the [v.1.0 tag](https://bitbucket.org/scilifelab-lts/reproducible_research_example/src/v1.0/) but option D) below will not work then, as `rocker/verse`, which the Docker image is based on, was removed from Dockerhub, so it is no longer possible to rebuild the image from the `Dockerfile`.*

**A.** Clone this repo, install software manually, run scripts manually:

```
#!bash

$ git clone https://bitbucket.org/scilifelab-lts/reproducible_research_example.git
$ # the rest is up to you...
```

**B.** Clone this repo, install required software manually:

```
#!bash

$ git clone https://bitbucket.org/scilifelab-lts/reproducible_research_example.git
$ cd reproducible_research_example
$ snakemake results/SRR068303.pdf
```

**C.** Clone this repo, use the conda environment file to facilitate software installation:

```
#!bash

$ git clone https://bitbucket.org/scilifelab-lts/reproducible_research_example.git
$ cd reproducible_research_example
$ conda env create -f conda_env.yml
$ conda activate my_conda_env
(my_conda_env)$ snakemake results/SRR068303.pdf

```
*In addition to the conda environment you will need some basic software that are typically not included through conda, like bzip2, git, wget and unzip, as well as latex. If you don't have latex, one simple option is to install it through the R package `tinytex`.*

**D.** Clone this repo, build and run Docker image from Dockerfile:

```
#!bash

$ git clone https://bitbucket.org/scilifelab-lts/reproducible_research_example.git
$ cd reproducible_research_example
$ docker build -t "my_docker_image" .
$ docker run -v $PWD:/home/reproducible_research_example/results my_docker_image
```

**E.** Use the snapshot image of this repo + system + software from Docker hub:

```
#!bash

$ docker pull scilifelablts/reproducible_research_example:v1.2
$ docker run -it -v $PWD:/home/reproducible_research_example/results \
	scilifelablts/reproducible_research_example:v1.2 /bin/bash
root@27344a656192# cd reproducible_research_example
root@27344a656192# conda activate my_conda_env
(my_conda_env)root@27344a656192# snakemake results/SRR068303.pdf
```
